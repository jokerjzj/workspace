说明:此文件用来存放可以复制黏贴直接使用的现成代码,格式如下:

1.代码效果

2.代码部分(分离的代码要补全基本结构)

3.如何使用

git 一般推送代码至云端:

<!-- 查看当前所在分支 -->

1.git branch

<!--新建并切换到新分支XXX-->

2.git checkout -b XXX

<!--  -->

3.git branch

<!--  -->

4.git status

<!--  -->

5.git add .

<!--  -->

6.git status

<!--  -->

7.git commit -m 'XXXXX'

<!--  -->

8.git status

<!--  -->

9.git branch

<!--推送到云端,第一次推送需要-u,origin是云端仓库别名,新建分支XXX-->

10.git push -u origin XXX

<!--  -->

11.git branch

<!--切换到主分支-->

12.git checkout master

<!--同步XXX分支到主分支-->

13.git merge XXX

<!--推送至云端-->

14.git push

安装 express:
在新文件夹安装 npm npm init -y
安装 express
npm i express -S

箭头函数和普通函数
箭头函数才能取到 vue,data 里面的 this.XXX 值
()=>{this.a}
function(){}

No.1 form 表单验证

1.代码效果

<img src="C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20200727145430509.png" alt="image-20200727145430509" style="zoom:80%;" />

2.代码部分

```vue
<!-- 内容主题区域 -->
<span>
        <!-- form表单要绑定数据,:model,绑定效验规则,:rules,ref是引用 -->
        <el-form :model="addForm" :rules="addFormRules" ref="addFormRef" label-width="70px">
          <!-- 通过prop来指定具体的校验规则 -->
          <el-form-item label="用户名" prop="username">
            <el-input v-model="addForm.username"></el-input>
          </el-form-item>
          <el-form-item label="密码" prop="password">
            <el-input v-model="addForm.password"></el-input>
          </el-form-item>
          <el-form-item label="邮箱" prop="email">
            <el-input v-model="addForm.email"></el-input>
          </el-form-item>
          <el-form-item label="手机" prop="mobile">
            <el-input v-model="addForm.mobile"></el-input>
          </el-form-item>
        </el-form>
      </span>
<script>
export default {
  data() {
    // 验证邮箱的规则
    var checkEmail = (rule, value, cb) => {
      // 验证邮箱的正则表达式
      const regEmail = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-])+/

      if (regEmail.test(value)) {
        // 合法的邮箱
        return cb()
      }

      cb(new Error('请输入合法的邮箱'))
    }

    // 验证手机号的规则
    var checkMobile = (rule, value, cb) => {
      // 验证手机号的正则表达式
      const regMobile = /^(0|86|17951)?(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$/

      if (regMobile.test(value)) {
        return cb()
      }

      cb(new Error('请输入合法的手机号'))
    }
     return {
      addDialogVisible: false,
      // 添加用户的表单数据
      addForm: {
        username: '',
        password: '',
        email: '',
        mobile: '',
      },
      // 添加用户表单验证规则
      addFormRules: {
        username: [
          {
            required: true,
            message: '请输入用户名',
            trigger: 'blur',
          },
          { min: 3, max: 10, message: '用户名长度在3~10个字符之间', trigger: 'blur' },
        ],
        password: [
          {
            required: true,
            message: '请输入密码',
            trigger: 'blur',
          },
          { min: 6, max: 15, message: '密码长度在6~15个字符之间', trigger: 'blur' },
        ],
        email: [
          {
            required: true,
            message: '请输入邮箱',
            trigger: 'blur',
          },
          { validator: checkEmail, trigger: 'blur' },
        ],
        mobile: [
          {
            required: true,
            message: '请输入手机号码',
            trigger: 'blur',
          },
          // validator指定具体校验规则
          { validator: checkMobile, trigger: 'blur' },
        ],
      },
    }
  },
</script>
```

3.<span>放入主体,<script>放入行为

No.2 表单的重置
<el-dialog title="添加用户" :visible.sync="addDialogVisible" width="50%" @close="addDialogClosed">
<!-- 内容主题区域 -->
<span>
<!-- form表单要绑定数据,:model,绑定效验规则,:rules,ref是引用 -->
<el-form :model="addForm" :rules="addFormRules" ref="addFormRef" label-width="70px">
</el-form>
</span>
methods:{
// 监听添加用户对话框的关闭事件
addDialogClosed(){
this.\$refs.addFormRef.resetFields()
}
}

No.3 汉字,字母,数字验证

jxName: [
{ required: true, min: 4, max: 40, message: '驾校名称限制 4 到 20 个字' },
{
required: true,
validator: (rule, value, callback) => {
const reg = /^[a-zA-Z0-9·\u4e00-\u9fa5]+\$/
if (value == '') {
callback(new Error('请填写必填项'))
} else if (!reg.test(value)) {
callback(new Error('只允许填写汉字、字母、数字'))
} else {
callback()
}
},
trigger: 'change'
}
