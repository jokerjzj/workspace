布林值需要v-bind绑定否则不生效

:deleteable=“false”

做github和自己的博客
删除功能要把自己删除掉



兄弟组件传值bus

查询页(查询,删除)
新增页(新增,修改)


1.查询页按钮新增,修改
加参数type='add',type='update'(带唯一标识)
created请求加载该条记录所有数据
2.通过路由跳转传递参数


发送后台请求

libs
 api.





1.home(新增页).index(查询页) 路由配置
2.页面优化(用vant引入组件(省市区))
3.增删改查功能实现



axios 的基本用法

  axios.get(‘/adata')
       .then(ret=>{
          // data属性名称是固定的，用于获取后台响应的数据
          console.log(ret.data)
       })



axios 的参数传递

GET传递参数

通过 URL 传递参数
通过 params 选项传递参数

axios.get(‘/adata?id=123')
       .then(ret=>{
          console.log(ret.data)
       })

axios.get(‘/adata/123')
       .then(ret=>{
          console.log(ret.data)
       })

  axios.get(‘/adata‘,{
          params: {
            id: 123
          }
       })
       .then(ret=>{
          console.log(ret.data)
       })

DELETE传递参数

axios.delete(‘/adata?id=123')
       .then(ret=>{
          console.log(ret.data)
       })

axios.delete(‘/adata‘,{
          params: {
            id: 123
          }
       })
       .then(ret=>{
          console.log(ret.data)
       })

POST传递参数

通过选项传递参数（默认传递的是 json 格式的数据）

axios.post(‘/adata',{
    uname: 'tom',
    pwd: 123
  }).then(ret=>{
    console.log(ret.data)
  })

通过 URLSearchParams 传递参数（application/x-www-form-urlencoded）

const params = new URLSearchParams();
   params.append('param1', 'value1');
   params.append('param2', 'value2');
   axios.post('/api/test', params).then(ret=>{
      console.log(ret.data)
   })

PUT传递参数

axios.put(‘/adata/123',{
    uname: 'tom',
    pwd: 123
  }).then(ret=>{
    console.log(ret.data)
  })

axios 的响应结果

响应结果的主要属性

data ： 实际响应回来的数据
headers ：响应头信息
status ：响应状态码
statusText ：响应状态信息

  axios.post('/axios-json‘).then(ret=>{
    console.log(ret)
  })

axios 的全局配置

axios.defaults.timeout = 3000;   // 超时时间
axios.defaults.baseURL = 'http://localhost:3000/app';  // 默认地址
axios.defaults.headers[‘mytoken’] = ‘aqwerwqwerqwer2ewrwe23eresdf23’// 设置请求头

axios拦截器

1. 请求拦截器

   在请求发出之前设置一些信息

   ![image-20200708110356891](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20200708110356891.png)

     //添加一个请求拦截器
     axios.interceptors.request.use(function(config){
       //在请求发出之前进行一些信息设置
       return config;
     },function(err){
       // 处理响应的错误信息
     });

   2. 响应拦截器

      在获取数据之前对数据做一些加工处理

      ![image-20200708110455130](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20200708110455130.png)

        //添加一个响应拦截器
        axios.interceptors.response.use(function(res){
          //在这里对返回的数据进行处理
          return res;
        },function(err){
          // 处理响应的错误信息
        })



   增加数据

   ​           axios处理数据供服务器识别                           接口

   1.页面收到用户数据---->2.数据提交发送给服务器---->3.服务器处理数据返回结果---->4.页面接受返回结果---->5.渲染到页面

   ​    axios的配置环境         axios请求拦截      axios.post



发送请求



1.引入axios

2.baseurl,延迟,请求头封装网络请求,封装拦截器

3.引入user.js, 创建请求数据方法

![image-20200708153159308](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20200708153159308.png)

4.方法引入页面,在methods里面用,

![image-20200708153543107](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20200708153543107.png)


在方法里面调用$router需要加this.指向全局,否则不识别$router

snipaste截图
