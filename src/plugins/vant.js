import Vue from 'vue'
// 用到哪个组件就按需导入
import {
  Button,
  Field,
  CellGroup,
  Cell,
  Checkbox
} from 'vant'

// 并在这里注册一下
Vue.use(Button)
Vue.use(Field)
Vue.use(Cell)
Vue.use(CellGroup)
Vue.use(Checkbox)

