// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
//移动端适配,还有postcss.config.js文件,
import './utils/rem'
import './assets/css/style.css'
// import Vant from 'vant';
import './plugins/vant'
import 'vant/lib/index.css';
import './plugins/element'
// import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import './assets/icon-font/iconfont.css'

// Vue.use(Vant);

// Vue.use(ElementUI);

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
