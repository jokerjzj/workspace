import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('@/components/home')
    },
    {
      path: '/addressList',
      name: 'addressList',
      component: () => import('@/components/addressList')
    },
    {
      path: '/delivery',
      name: 'delivery',
      component: () => import('@/components/delivery')
    },
  ]
})
